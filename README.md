## Project Name & Pitch

#### Book Shop test task:

An application used to immitate the online service for the book rent,   
built with React, Redux , JavaScript, and CSS.

## Project Status

This project is currently in development.  
- Book list api, state management and component ready.  
      - click handlers todo
- Tests started  
      - Snapshots for basic components added

## Project Screen Shot(s)

...coming

## Installation and Setup Instructions

#### Example:  

Clone down this repository. You will need `node` and `npm` installed globally on your machine.  

Installation:

`npm install`  

To Run Test Suite:  

`npm test`  

To Start Server:

`npm start`  

To Visit App:

[Book Shop Test](https://test1285.gitlab.io/book-shop)
