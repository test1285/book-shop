import React from 'react';
import {AppRouteNamesList, IRouterRoute} from 'models';
import {AppRouteList} from '../AppRouteList';

export function addComponentsToPageRoutesList(
  componentToRender: Partial<Record<AppRouteNamesList, React.ElementType>>,
) {
  return Object.keys(componentToRender)
    .map(key => {
      const route = AppRouteList.find(({ name }) => name === key);

      return {
        ...route,
        Component: componentToRender[key as AppRouteNamesList],
      }
    }).filter(route => !!route.Component) as IRouterRoute[];
}
