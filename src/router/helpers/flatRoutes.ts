import {IRoute} from '../../models';

export function flatRoutes(route: IRoute): IRoute[] {
  const basePath = route.path;
  return [
    {
      path: basePath,
      name: route.name,
    },
    ...(route.nested || []).map(({ name, path, }) => ({
      path: basePath + path,
      name,
    })),
  ];
}
