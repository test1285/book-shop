import {BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom';
import React from 'react';
import {AppPathList} from './AppPathList';
import {Main} from '../components/main';

const BooksRouter = React.lazy(() => import('../pages/books/router/books.router'));
const CartRouter = React.lazy(() => import('../pages/cart/router/cart.router'));


export default function AppRouter() {
  return (
    <Router basename={process.env.PUBLIC_URL}>
      <Main>
        <Switch>
          <Route path={AppPathList.books} component={BooksRouter}/>
          <Route path={AppPathList.cart} component={CartRouter}/>
          <Redirect to={AppPathList.books} />
        </Switch>
      </Main>
    </Router>
  );
}
