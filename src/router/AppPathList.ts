import {AppRouteList} from './AppRouteList';
import {flatRoutes} from './helpers';
import {AppRouteNamesList} from '../models';

export const AppPathList = AppRouteList
  .map(routeList => flatRoutes(routeList))
  .flat()
  .reduce((res, { name, path }) => ({
    ...res,
    [name]: path,
  }), {}) as Record<AppRouteNamesList, string>;
