import {booksRouteList} from 'pages/books/router';
import {cartRouteList} from 'pages/cart/router';

export const AppRouteList = [
  booksRouteList,
  cartRouteList,
];
