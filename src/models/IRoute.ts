import {AppRouteNamesList} from './AppRouteNamesList';
import React from 'react';

export interface IRouterRoute extends IRoute {
  Component: React.ElementType;
}

export interface IRoute extends IFlatRoute {
  nested?: IRoute[];
}

export interface IFlatRoute {
  path: string;
  name: AppRouteNamesList;
}
