import {decapitalizeFirstLetter} from './decapitalizeFirstLetter';

export function decapitalizeObjectKeys(): undefined;
export function decapitalizeObjectKeys<T, R>(src: T): R;

export function decapitalizeObjectKeys<T, R>(src?: T) {
  if (!src) return src;
  let key: string;

  if (Array.isArray(src)) {
    return src.map(i => decapitalizeObjectKeys(i))
  } else {
    const keys = Object.keys(src);
    let n = keys.length;
    const newObj: Record<string, any> = {};

    while (n--) {
      key = keys[n];

      if (key === 'ID') {
        newObj.id = src[key as keyof T];
      } else {
        newObj[decapitalizeFirstLetter(key)] = src[key as keyof T];
      }
    }

    return newObj as R;
  }
}


