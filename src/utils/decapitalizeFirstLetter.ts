export function decapitalizeFirstLetter(): undefined;
export function decapitalizeFirstLetter<T>(str: T): T;

export function decapitalizeFirstLetter<T>(str?: T) {
  if (!str || typeof str !== 'string') return str;

  return str.slice(0,1).toLowerCase() + str.slice(1);
}
