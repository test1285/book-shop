import {BookGenreTypes} from 'pages/books/models';

interface IBookRentSpecification {
  perDay: number;
  minPeriod?: number;
  minPeriodCost?: number;
}

export const BookRentSpecification: Record<BookGenreTypes, IBookRentSpecification> = {
  [BookGenreTypes.regular]: {
    perDay: 1.5,
    minPeriod: 2,
    minPeriodCost: 2,
  },
  [BookGenreTypes.novel]: {
    perDay: 1.5,
    minPeriod: 3,
  },
  [BookGenreTypes.fiction]: {
    perDay: 3
  },
};
