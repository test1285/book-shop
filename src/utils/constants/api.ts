export const BASE_API_URL = 'https://fakerestapi.azurewebsites.net/api';

export const BOOKS_API_PATHS = {
  books: 'Books',
}

export enum ApiErrors {
  TECHNICAL_ERROR = 'TECHNICAL_ERROR',
  SERVER_ERROR = 'SERVER_ERROR',
}
