import {IBookFormatted, BookGenreTypes, IBookList} from 'pages/books/models';

export function addFakeValuesToBookList(bookList: IBookFormatted[]): IBookList {
  return bookList.map((book, i) => addFakeValuesToBook(book, i))
}

const IDX_DIVIDERS = {
  fiction: 4,
  novel: 3,
};

function addFakeValuesToBook(book: IBookFormatted, i: number) {
  return {
    ...book,
    genre: getBookGenre(i),
  }
}

function getBookGenre(idx: number) {
  if (getGenreByDivider(idx, IDX_DIVIDERS.fiction)) {
    return BookGenreTypes.fiction;
  }
  if (getGenreByDivider(idx, IDX_DIVIDERS.novel)) {
    return BookGenreTypes.novel;
  }
  return BookGenreTypes.regular;
}

function getGenreByDivider(idx: number, divider: number) {
  return idx >= divider && idx%divider === 0
}


