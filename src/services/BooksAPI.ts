import {request} from 'helpers';
import {BOOKS_API_PATHS} from 'utils';

export const BooksApi = {
  getBooks: <T>() => {
    const params = {
      url: BOOKS_API_PATHS.books,
    };

    return request<T>(params);
  }
};
