import React from 'react';
import {Provider} from 'react-redux';
import {configureStore} from 'store';
import './App.css';

const store = configureStore();

const AppRouter = React.lazy(() => import('./router/router'));

function App() {
  return (
    <React.Suspense fallback={'...loading'}>
      <Provider store={store}>
        <AppRouter />
      </Provider>
    </React.Suspense>
  );
}

export default App;
