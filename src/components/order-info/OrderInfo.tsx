import React from 'react';
import {Link} from 'react-router-dom';
import {AppPathList} from '../../router/AppPathList';
import {ShoppingCartIcon} from '../icons';
import {useSelector} from 'react-redux';
import {orderedBooksSelector} from 'pages/cart/selectors';
import './orderInfo.styles.scss';

export const OrderInfo = () => {
  const orderedBooksCount = useSelector(orderedBooksSelector).length;

  return (
    <div className="order-info">
      <span>Books:</span>
      <span
        key={orderedBooksCount}
        className={'order-info__count scale-bounce'}
      >
        {orderedBooksCount}
      </span>
      <Link to={AppPathList.cart} className="order-info__link">
        <ShoppingCartIcon iconClass="order-info__icon"/>
      </Link>
      </div>
  );
};
