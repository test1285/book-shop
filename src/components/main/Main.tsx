import React from 'react';
import {Header} from '../header';
import './main.styles.scss';

export const Main: React.FC = ({children}) => {
  return (
    <div className="app-wrapper">
      <Header />
      <main>
        {children}
      </main>
    </div>
  );
};
