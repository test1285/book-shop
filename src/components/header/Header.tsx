import React from "react";
import { Link, useLocation } from "react-router-dom";
import { AppPathList } from "../../router/AppPathList";
import "./header.styles.scss";
import { OrderInfo } from "../order-info";
import {Button} from '@material-ui/core';

export const Header = () => {
  const { pathname } = useLocation();
  const homePage = AppPathList.books;
  return (
    <div className="header">
      {pathname !== homePage && (
        <Link to={AppPathList.books} className="header__link">
          <Button
            variant="contained"
            color="primary"
          >
            To books list
          </Button>
        </Link>
      )}
      <OrderInfo />
    </div>
  );
};
