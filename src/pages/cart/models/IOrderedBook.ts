import {IBook} from 'pages/books/models';

export interface IOrderedBook extends IBook {
  rentEndDate: string;
  cost?: number;
}
