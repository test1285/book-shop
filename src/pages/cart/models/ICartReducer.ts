import {IOrder} from './IOrder';
import {IBaseReducer} from 'store/models';

export interface ICartReducer extends IBaseReducer {
  order: IOrder;
}
