import {BookGenreTypes} from 'pages/books/models';
import {BookRentSpecification} from 'utils';

export function calculateBookRentCost(
  rentPeriodCountInDays: number,
  bookGenre: BookGenreTypes,
) {
  const {perDay: costPerDay, minPeriod = 0, minPeriodCost} = BookRentSpecification[bookGenre];

  if (minPeriod && rentPeriodCountInDays <= minPeriod) {
    return minPeriodCost || getRentCost(minPeriod, costPerDay);
  }

  if (minPeriodCost) {
    const daysAfterMinPeriod = rentPeriodCountInDays - minPeriod;
    return minPeriodCost + getRentCost(daysAfterMinPeriod, costPerDay)
  }

  return getRentCost(rentPeriodCountInDays, costPerDay);
}

function getRentCost(daysCount: number, costPerDay: number) {
  return daysCount * costPerDay;
}
