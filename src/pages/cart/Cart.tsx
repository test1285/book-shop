import React from "react";
import { OrderList } from "./components";

const Cart = () => {
  return (
    <div>
      <h2>Order</h2>
      <OrderList />
    </div>
  );
};

export default Cart;
