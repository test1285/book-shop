import { injectReducer } from "store/helpers";
import { ICartReducer } from "./models";
import * as types from "./ActionTypes";
import { successActionHandler } from "../../store/models";

export const InitialCartReducer: ICartReducer = {
  order: {
    books: [],
  },
};

export default injectReducer(InitialCartReducer, {
  [types.ADD_BOOK_TO_CART]: (
    state: ICartReducer,
    action: { payload: number }
  ) => {
    return {
      ...successActionHandler(state),
      order: {
        books: !state.order.books.includes(action.payload)
          ? [...state.order.books, action.payload]
          : state.order.books,
      },
    };
  },

  [types.REMOVE_BOOK]: (state: ICartReducer, action: { payload: number }) => {
    return {
      ...state,
      order: {
        books: state.order.books.filter((item) => item !== action.payload),
      },
    };
  },
});
