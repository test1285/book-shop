import { createAction } from "store";
import * as types from "./ActionTypes";

export const addBookToCart = createAction(types.ADD_BOOK_TO_CART);
export const removeBook = createAction(types.REMOVE_BOOK);
