import {IAppState} from 'store/models';
import {cartStateSelector} from './cartStateSelector';

export function orderSelector(state: IAppState) {
  return cartStateSelector(state).order;
}
