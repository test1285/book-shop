import {orderSelector} from './orderSelector';
import {createSelector} from 'reselect';

export const orderedBooksSelector = createSelector(
  orderSelector,
  ({ books }) => books
);
