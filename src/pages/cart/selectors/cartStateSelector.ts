import {IAppState} from 'store/models';

export function cartStateSelector(state: IAppState) {
  return state.cart;
}
