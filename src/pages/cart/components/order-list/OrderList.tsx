import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {bookListSelector} from "pages/books/selectors";
import {IBookList} from "pages/books/models";
import {getNextDay} from 'helpers';
import {differenceInDays} from "date-fns";
import "./orderList.styles.scss";
import {List} from '@material-ui/core';
import {orderedBooksSelector} from '../../selectors';
import {IOrderedBook} from '../../models'
import {OrderedListItem} from '../order-list-item';
import {calculateBookRentCost} from "../../helpers";
import {removeBook} from '../../cart.actions';

export const OrderList = () => {
  const dispatch = useDispatch();

  const [orderedBooks, setOrderedBooks] = React.useState<IOrderedBook[]>([]);

  const orderedBookIds = useSelector(orderedBooksSelector);
  const bookList = useSelector(bookListSelector);

  const tomorrow = React.useMemo(() => getNextDay(), []);

  React.useEffect(() => {
    setOrderedBooks(updateOrderedBooks(bookList));
  }, [bookList]);

  React.useEffect(() => {
    setOrderedBooks(prevState => updateOrderedBooks(prevState))
  }, [orderedBookIds]);

  const handleChangeRentPeriod = React.useCallback(
    (id: number, newDate: string) =>
      setOrderedBooks((orderedBooks) => {
        return orderedBooks?.map((book) =>
          book.id === id
            ? {
              ...book,
              rentEndDate: newDate,
              cost: calculateBookRentCost(
                differenceInDays(getNextDay(newDate, false), new Date),
                book.genre
              ),
            } : book
        );
      }),
    []
  );

  const handleRemoveBook = React.useCallback(
    (id: number) => dispatch(removeBook(id)),
    []
  );

  const updateOrderedBooks = React.useCallback(
    (list?: IOrderedBook[] | IBookList) => {
      if (!orderedBookIds.length || !list?.length) {
        return [];
      }

      return list
        .filter(({id}) => orderedBookIds.includes(id))
        .map((book) => {
          const rentEndDate = book?.rentEndDate || tomorrow;
          const cost = calculateBookRentCost(
            differenceInDays(getNextDay(rentEndDate, false), new Date),
            book.genre
          );

          return {
            ...book,
            rentEndDate,
            cost,
          }
        });
    }, [orderedBookIds]
  );

  const totalCost = React.useMemo(() => {
    return orderedBooks.reduce((res: number, {cost = 0}: IOrderedBook) => res + cost, 0);
  }, [orderedBooks]);

  return (
    <>
      <List aria-label="main mailbox folders">
        {orderedBooks &&
        orderedBooks.map((book) => (
          <OrderedListItem
            {...book}
            onChangeRentEndDate={handleChangeRentPeriod}
            onRemoveBook={handleRemoveBook}
            key={book.id}
          />
        ))}
      </List>
      <p className="order-total">
        <span className="order-total__text">{`Total: ${totalCost} $`}</span>
      </p>
    </>
  );
};
