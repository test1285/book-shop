import React from 'react';
import {Delete as DeleteIcon} from '@material-ui/icons'
import {Avatar, IconButton, ListItem, ListItemAvatar, ListItemText} from '@material-ui/core';
import {add, format} from "date-fns";
import {DATE_FORMATS} from 'utils';
import {IOrderedBook} from '../../models'
import './orderListItem.styles.scss';


interface OrderListItemProps extends IOrderedBook {
  onChangeRentEndDate: (id: number, date: string) => void;
  onRemoveBook: (id: number) => void;
}

export function OrderedListItem({
                                  title,
                                  id,
                                  onChangeRentEndDate,
                                  onRemoveBook,
                                  rentEndDate,
                                  genre,
                                  cost = 0,
                                }: OrderListItemProps) {

  function handleChangeDate(e: React.ChangeEvent<HTMLInputElement>) {
    onChangeRentEndDate(id, e.target.value);
  }

  function handleRemoveBook() {
    onRemoveBook(id);
  }

  return (
    <ListItem className="order-list-item">
      <IconButton
        aria-label="delete"
        size="small"
        color="secondary"
        className="icon-delete"
        onClick={handleRemoveBook}
      >
        <DeleteIcon fontSize="small"/>
      </IconButton>
      <ListItemAvatar>
        <Avatar>
          <img src="https://picsum.photos/200/300" alt={`${title} logo`}/>
        </Avatar>
      </ListItemAvatar>
      <ListItemText className="item-info">
        {`${title}, ${genre}`}
      </ListItemText>
      <ListItemText className="item-price">
        {`Rent cost: ${cost}`}
      </ListItemText>
      <input
        type="date"
        className="order-list__item-input"
        value={rentEndDate}
        onChange={handleChangeDate}
        min={format(
          add(new Date(), {
            days: 1,
          }),
          DATE_FORMATS.YYYY_MM_DD
        )}
      />
    </ListItem>
  );
}
