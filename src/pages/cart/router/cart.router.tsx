import React from 'react';
import {RouteComponentProps, Switch, Route, RouteProps} from 'react-router-dom';
import {AppRouteNamesList} from 'models';
import {addComponentsToPageRoutesList} from 'router';

const Cart = React.lazy(() => import('../Cart'));

const mapComponents = {
  [AppRouteNamesList.cart]: Cart,
};

const booksRouterRoutes = addComponentsToPageRoutesList(mapComponents);

const CartRouter: React.FC<RouteComponentProps> = () => {
  return (
    <Switch>
      {
        booksRouterRoutes.map(({path, Component}) => (
          <Route
            key={path}
            exact
            path={path}
            render={(props: RouteProps) => <Component {...props} />}
          />
        ))
      }
    </Switch>
  )
};

export default CartRouter;
