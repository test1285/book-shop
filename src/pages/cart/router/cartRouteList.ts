import {IRoute, AppRouteNamesList} from 'models';

enum CartPathList {
  cart = '/cart',
}

export const cartRouteList: IRoute = {
  path: CartPathList.cart,
  name: AppRouteNamesList.cart,
};
