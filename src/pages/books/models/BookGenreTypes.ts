export enum BookGenreTypes {
  fiction = 'fiction',
  novel = 'novel',
  regular = 'regular',
}
