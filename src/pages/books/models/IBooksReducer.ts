import {IBaseReducer} from 'store/models';
import {IBookList} from './IBookFormatted';

export interface IBooksReducer extends IBaseReducer {
  bookList?: IBookList,
}
