import {IsoString} from 'models';
import {BookGenreTypes} from './BookGenreTypes';
export interface IBookList extends Array<IBook> {}

export interface IBook extends Record<string, any>{
  id: number;
  title: string;
  description: string;
  pageCount: number;
  excerpt: string;
  publishDate: IsoString;
  genre: BookGenreTypes;
}

export interface IBookFormatted {
  id: number;
  title: string;
  description: string;
  pageCount: number;
  excerpt: string;
  publishDate: IsoString;
}

export interface IBookDB {
  ID: number;
  Title: string;
  Description: string;
  PageCount: number;
  Excerpt: string;
  PublishDate: IsoString;
}
