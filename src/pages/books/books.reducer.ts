import { injectReducer } from "store/helpers";
import { IBookList, IBooksReducer } from "./models";
import * as types from "./ActionTypes";
import {
  requestActionHandler,
  failureActionHandler,
  successActionHandler,
} from "store";

export const InitialBooksReducer: IBooksReducer = {};

export default injectReducer(InitialBooksReducer, {
  [types.GET_BOOK_LIST.REQUEST]: requestActionHandler,

  [types.GET_BOOK_LIST.FAILURE]: failureActionHandler,

  [types.GET_BOOK_LIST.SUCCESS]: (
    state: IBooksReducer,
    action: { payload: IBookList }
  ) => {
    return {
      ...successActionHandler(state),
      bookList: action.payload,
    };
  },
});
