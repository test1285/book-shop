import {IAppState} from 'store/models';

export function booksStateSelector(state: IAppState) {
  return state.books;
}
