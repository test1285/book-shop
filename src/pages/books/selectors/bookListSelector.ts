import { createSelector } from "reselect";

import { booksStateSelector } from "./booksStateSelector";

export const bookListSelector = createSelector(
  booksStateSelector,
  ({ bookList }) => bookList
);
