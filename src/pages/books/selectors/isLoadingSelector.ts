import {IAppState} from 'store/models';

export function isLoadingSelector(state: IAppState) {
  return state.books.isLoading;
}
