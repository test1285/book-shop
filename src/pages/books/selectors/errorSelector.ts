import {IAppState} from 'store/models';

export function errorSelector(state: IAppState) {
  return state.books.error;
}
