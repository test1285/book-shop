import {createApiAction} from 'store';
import * as types from './ActionTypes';
import {IBookList} from './models';

export const getBooks = createApiAction<IBookList>(types.GET_BOOK_LIST);
