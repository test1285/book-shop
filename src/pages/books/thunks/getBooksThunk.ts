import {Dispatch} from 'redux';
import {BooksApi} from 'services';
import {logRequestError} from 'helpers';
import {addFakeValuesToBookList, decapitalizeObjectKeys} from 'utils';
import {getBooks} from '../books.actions';
import {IBookDB, IBookFormatted} from '../models';

export const getBooksThunk = () => (dispatch: Dispatch) => {
  dispatch(getBooks.loading());

  BooksApi.getBooks<IBookDB[]>()
    .then(bookList => {
      const normalizedBooksList = decapitalizeObjectKeys<IBookDB[], IBookFormatted[]>(bookList);
      const booksList = addFakeValuesToBookList(normalizedBooksList);
      dispatch(getBooks.success(booksList))
    })
    .catch((e) => {
      logRequestError(e, 'getBooks');
      dispatch(getBooks.fail(e))
    });
};
