import React from "react";
import { IBook, IBookList } from "../../models";
import {
  Button,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  Grid,
  Typography,
} from "@material-ui/core";
import "./bookList.styles.scss";
import { BookRentSpecification } from "utils/constants/books";

interface BooksListProps {
  bookList?: IBookList;
  onAddBookToCart: (id: number) => void;
}

export const BooksList = React.memo(
  ({ bookList, onAddBookToCart }: BooksListProps) => {
    return bookList?.length ? (
      <Grid container spacing={3} className="book-list">
        {bookList.map((book, idx) => (
          <Grid item key={idx} xs={12} sm={6} md={4} lg={3}>
            <BooksListItem {...book} onAddBookToCart={onAddBookToCart} />
          </Grid>
        ))}
      </Grid>
    ) : (
      <p>'No books yet, come back later please...'</p>
    );
  }
);

interface BooksListItemProps extends IBook {
  idx?: number;
  onAddBookToCart: (id: number) => void;
}

function BooksListItem({
  description,
  excerpt,
  id,
  pageCount,
  publishDate,
  title,
  genre,
  onAddBookToCart,
}: BooksListItemProps) {
  const handleClickAddToCartBtn = React.useCallback(() => onAddBookToCart(id), [
    id,
  ]);

  return (
    <Card className="book-list-item">
      <CardActionArea>
        <CardMedia
          className="book-card__img"
          image="https://picsum.photos/200/300"
          title={title}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {title}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {`Genre: ${genre}`}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {`Rent cost: ${BookRentSpecification[genre].perDay}`}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button size="small" color="primary" onClick={handleClickAddToCartBtn}>
          Add to cart
        </Button>
      </CardActions>
    </Card>
  );
}
