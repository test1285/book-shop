import { BooksList } from "../BooksList";
import { shallowToJson } from "enzyme-to-json";
import { shallow } from "enzyme";
import * as React from "react";
import {BookGenreTypes} from 'pages/books/models';

test("basic snapshot", () => {
  const component = shallow(<BooksList onAddBookToCart={() => {}} />);

  expect(shallowToJson(component)).toMatchSnapshot();
});

test("with booklist", () => {
  const bookList = [
    {
      id: 1,
      title: "1",
      description: "1",
      pageCount: 1,
      excerpt: "1",
      publishDate: "2012-03-17T14:21:43+0000",
      genre: BookGenreTypes.fiction,
    },
  ];
  const component = shallow(<BooksList bookList={bookList} onAddBookToCart={() => {}} />);

  expect(shallowToJson(component)).toMatchSnapshot();
});

