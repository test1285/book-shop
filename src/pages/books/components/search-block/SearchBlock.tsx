import React from 'react';
import {CustomInput} from 'shared';
import {useDebounce} from 'hooks';
import './SerachField.styles.scss';

interface SearchBlockProps {
  filterValue?: string;
  onChange: (f: string) => void;
}

export const SearchBlock = ({
                       filterValue = '',
                       onChange,
                     }: SearchBlockProps) => {

  const [filter, setFilter] = React.useState(() => filterValue);
  const debouncedFilter = useDebounce(filter, 300);

  const handleFilterChange = React.useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => setFilter(e.target.value),
    []
  );

  React.useEffect(
    () => onChange(debouncedFilter),
    [debouncedFilter]
  );

  return (
    <div className="search-block">
      <CustomInput
        value={filter}
        onChange={handleFilterChange}
        wrapperClass="search-field"
        label="Search books by title"
      />
    </div>
  );
};
