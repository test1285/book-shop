import { SearchBook } from "../SearchBook";
import { shallowToJson } from "enzyme-to-json";
import { shallow } from "enzyme";
import * as React from "react";

test("basic snapshot", () => {
  const component = shallow(
    <SearchBook onChange={() => {}} onSubmit={() => {}} disabled />
  );

  expect(shallowToJson(component)).toMatchSnapshot();
});
