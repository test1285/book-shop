import { createApiActionTypes } from "store/helpers";

export const GET_BOOK_LIST = createApiActionTypes("GET_BOOK_LIST");
