import React from 'react';
import {RouteComponentProps, Switch, Route, RouteProps} from 'react-router-dom';
import {AppRouteNamesList} from 'models';
import {addComponentsToPageRoutesList} from 'router';

const Books = React.lazy(() => import('../Books'));

const mapComponents = {
  [AppRouteNamesList.books]: Books,
};

const booksRouterRoutes = addComponentsToPageRoutesList(mapComponents);

const BooksRouter: React.FC<RouteComponentProps> = () => {
  return (
    <Switch>
      {
        booksRouterRoutes.map(({path, Component}) => (
          <Route
            key={path}
            exact
            path={path}
            render={(props: RouteProps) => <Component {...props} />}
          />
        ))
      }
    </Switch>
  )
};

export default BooksRouter;
