import {IRoute, AppRouteNamesList} from 'models';

enum BooksPathList {
  books = '/books',
}

export const booksRouteList: IRoute = {
  path: BooksPathList.books,
  name: AppRouteNamesList.books,
};
