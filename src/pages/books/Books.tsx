import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {filterArrayByFieldValue} from 'helpers';
import {getBooksThunk} from "./thunks";
import {bookListSelector} from "./selectors";
import {BooksList} from "./components/books-list";
import {addBookToCart} from "../cart/cart.actions";
import {SearchBlock} from './components';

const Books = () => {
  const dispatch = useDispatch();

  const bookList = useSelector(bookListSelector);
  const [filter, setFilter] = React.useState();

  React.useEffect(() => {
    dispatch(getBooksThunk());
  }, [dispatch]);

  const handleAddBookToCart = React.useCallback(
    (id: number) => dispatch(addBookToCart(id)),
    [dispatch]
  );

  const handleFilterChange = React.useCallback(
    (filter: string) => setFilter(filter),
    []
  );

  const filteredBookList = React.useMemo(
    () => filterArrayByFieldValue('title', bookList, filter),
    [filter]
  );

  return (
    <>
      <SearchBlock
        onChange={handleFilterChange}
      />
      <BooksList
        bookList={filteredBookList || bookList}
        onAddBookToCart={handleAddBookToCart}
      />
    </>
  );
};

export default Books;
