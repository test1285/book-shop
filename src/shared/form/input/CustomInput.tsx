import React from 'react';
import {TextField} from '@material-ui/core';
import cn from 'classnames';
import {StandardTextFieldProps} from '@material-ui/core/TextField/TextField';

interface CustomInputProps extends Partial<StandardTextFieldProps> {
  wrapperClass?: string;
}

export const CustomInput = ({
                       value,
                       onChange,
                       wrapperClass,
                       name,
                       label,
                       id,
                     }: CustomInputProps) => {
  return (
    <TextField
      id={id}
      label={label}
      name={name}
      className={cn(wrapperClass)}
      value={value}
      onChange={onChange}
      fullWidth
    />
  );
};
