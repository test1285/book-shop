import { CustomInput } from '../CustomInput';
import { shallowToJson} from 'enzyme-to-json';
import { shallow } from 'enzyme';
import * as React from 'react';

test("basic snapshot", () => {
  const component = shallow(<CustomInput value="testValue"/>);

  expect(shallowToJson(component)).toMatchSnapshot();
});
