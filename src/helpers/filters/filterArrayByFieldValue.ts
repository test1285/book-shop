import {isString} from '../typeguards';

export function filterArrayByFieldValue<T, K extends keyof T>(
  key: K, array ?: Array<T>, filterValue ?: string
): Array<T> | undefined {
  if (!filterValue || !array?.length) return array;

  const filterInLowerCase = filterValue.toLowerCase();

  return array.filter(item => {
    const prop = item[key];
    return  isString(prop) && prop.toLowerCase().includes(filterInLowerCase);
  });
}
