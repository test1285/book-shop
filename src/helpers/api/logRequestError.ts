export function logRequestError(e: Error, action: string) {
  return console.warn(`> Failed to ${action}: ${e.message}`)
}
