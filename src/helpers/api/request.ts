import axios, {Method, AxiosRequestConfig, AxiosError, AxiosResponse} from 'axios';
import {ApiErrors, BASE_API_URL} from 'utils';

export interface IRequestParams {
  url: string;
  method?: Method;
  data?: Record<string, unknown>;
  params?: Record<string, unknown>;
}

const defaultHeaders = {
  'content-type': 'application/json',
};

const axiosInstance = axios.create({
  timeout: 25000,
  baseURL: BASE_API_URL,
  headers: defaultHeaders,
});

export const request = async <T>(conditions: IRequestParams): Promise<T> => {

  const {
    url,
    data,
    method = 'GET',
    params,
  } = conditions;

  const options: AxiosRequestConfig = {
    url,
    data,
    params,
    method,
  };

  return axiosInstance(options)
    .then(res => res.data)
    .catch((error: AxiosError) => {
      /*  connection troubles */
      if (!error.response) {
        throw new Error(ApiErrors.TECHNICAL_ERROR);
      }
      /*  server troubles */
      if (error.response.status === 500) {
        throw new Error(ApiErrors.SERVER_ERROR);
      }

      throw error.response.data;
    })
};
