import {add, format} from "date-fns";
import {DATE_FORMATS} from 'utils';

export function getNextDay() :string
export function getNextDay(d: string, f: false) : Date

export function getNextDay(
  date?: string,
  shouldFormat: boolean = true,
  formatType: string = DATE_FORMATS.YYYY_MM_DD
) {
  const nextDay = add(date ? new Date(date) : new Date(), {
    days: 1,
  });
  if (shouldFormat) {
    return format(nextDay, formatType)
  }
  return nextDay;
}
