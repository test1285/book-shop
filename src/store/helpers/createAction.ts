export function createAction(type: string) {
  return function action<Payload>(payload?: Payload, ...rest: any[]) {
    return {
      type,
      payload,
      ...rest,
    }
  }
}
