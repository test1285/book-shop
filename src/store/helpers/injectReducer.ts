import {IAppState} from '../models';
import {IAction} from '../models/IAction';

export function injectReducer<T>(
  initialState: T,
  handlers: Record<IAction<unknown>['type'], (s: T, a: any) => T>
) {
  return (state = initialState, action: IAction<unknown>) => {
    if (action.hasOwnProperty('type') && handlers[action.type]) {
      return handlers[action.type](state, action) as unknown as T;
    }
    return state as unknown as T;
  }
};
