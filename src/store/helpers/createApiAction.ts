import {IApiAction, IApiActionTypes} from '../models/IApiAction';
import {createAction} from './createAction';

export function createApiAction<Payload = {}, SuccessData = {}, Error = {}>(
  type: IApiActionTypes
): IApiAction<Payload, SuccessData, Error> {
  return {
    default: createAction(type.DEFAULT),
    loading: createAction(type.REQUEST),
    success: createAction(type.SUCCESS),
    fail: createAction(type.FAILURE),
  }
}

export function createApiActionTypes(base: string): IApiActionTypes {
  return {
    DEFAULT: `base`,
    REQUEST: `${base}_REQUEST`,
    SUCCESS: `${base}_SUCCESS`,
    FAILURE: `${base}_FAILURE`,
  }
}
