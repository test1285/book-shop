import { combineReducers } from 'redux';
import books from 'pages/books/books.reducer';
import cart from 'pages/cart/cart.reducer';

export default combineReducers({
  books,
  cart,
});
