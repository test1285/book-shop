import {IAppState} from './models';

import { InitialBooksReducer } from 'pages/books/books.reducer';
import { InitialCartReducer } from 'pages/cart/cart.reducer';

export const InitialState: IAppState = {
  books: InitialBooksReducer,
  cart: InitialCartReducer,
};
