import {IBooksReducer} from 'pages/books/models';
import {ICartReducer} from 'pages/cart/models';

export interface IAppState {
  books: IBooksReducer;
  cart: ICartReducer
}
