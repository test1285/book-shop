import {IAction} from './IAction';
import {IActionFunction} from './IActionFunction';

export interface IApiAction<Payload, SuccessData, ErrorMessage> {
  default: IActionFunction<Payload, IAction<Payload>>;
  loading: IActionFunction<Payload, IAction<Payload>>;
  success: IActionFunction<Payload, IAction<Payload>>;
  fail: IActionFunction<Payload, IAction<Payload>>;
}

export interface IApiActionTypes {
  DEFAULT: string;
  REQUEST: string;
  SUCCESS: string;
  FAILURE: string;
}
