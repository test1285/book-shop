export function requestActionHandler<T>(state: T): T {
  return {
    ...state,
    isLoading: true,
  }
}
