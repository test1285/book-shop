export interface IAction<Payload> extends Object {
  type: string;
  payload?: Payload;
  error?: Error;
}
