export interface IBaseReducer {
  isLoading?: boolean;
  error?: Error | { Message: string };
}
