export type IActionFunction<T, R> = (t?: T) => R;
