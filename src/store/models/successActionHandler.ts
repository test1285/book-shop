export function successActionHandler<T>(state: T): T {
  return {
    ...state,
    error: undefined,
    isLoading: false,
  }
}
