import {IAppState} from './IAppState';
import {requestActionHandler} from './requestActionHandler';

export function failureActionHandler<T>(
  state: T,
  { payload }: { payload: Error},
  ...rest: Record<string, unknown>[]
  ): T {
  return {
    ...requestActionHandler(state),
    error: payload,
    ...rest,
  }
}
