import {applyMiddleware, createStore, Store} from 'redux';
import {IAppState} from './models';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import reducers from './combineReducers';
import {InitialState} from './InitialState';

export function configureStore(): Store<IAppState> {
  const enhancer = composeWithDevTools(applyMiddleware(thunk));

  // @ts-ignore
  return createStore(reducers, InitialState, enhancer);
}
